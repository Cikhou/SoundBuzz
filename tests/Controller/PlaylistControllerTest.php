<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PlaylistControllerTest extends WebTestCase { 
    
    
    public function testGetPlaylists(){

    $client = static::getPlaylists();

    $client->request('GET', 'http://127.0.0.1:8001/playlists');

    $this->assertEquals(200, $client->getResponse()->getStatusCode());

    $crawler = $client->request('GET', 'http://127.0.0.1:8001/playlists');
    }
}
