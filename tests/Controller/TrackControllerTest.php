<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TrackControllerTest extends WebTestCase { 
    
    
    public function testAddTrack(){

    $client = static::addTrack();
    $$client->request(
        'POST',
        'http://127.0.0.1:8001/addtrack',
        array(),
        array(),
        array('CONTENT_TYPE' => 'application/json'),
        '{"title":"hola"}',
        '{"artist":"muchachos"}',
        '{"length":"50:00"}'  );


    $this->assertEquals(401, $client->getResponse()->getStatusCode());

    }
    public function testDeleteTrack(){

        $client = static::removeTrack();
        $client->request(
            'DELETE',
            '/removetrack/1',
            array(),
            array(),
            array('PHP_AUTH_USER' => 'admin', 'PHP_AUTH_PW' => 'admin')
        );
    
    
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    
        }
}
