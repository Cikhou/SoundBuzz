<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase{
    
    public function testFormRegister() {
    
    $client = static::adduser();
        
    $client->request('POST', 'http://127.0.0.1:8001/adduser');    

    $form = $crawler->selectButton('submit')->form();

    
    $form['name'] = 'jesue"àé)';
    $form['password'] = 'puissance';
    $form['email'] = 'voila@gmail.com';
    $form['form_name[subject]'] = 'Hey there!';
    
    
    $crawler = $client->submit($form);

    $this->assertContains('error', $client->getResponse()->getContent());
    }

}