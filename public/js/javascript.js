function update(player) {
    var duration = player.duration;    // Durée totale
    var time     = player.currentTime; // Temps écoulé
    var fraction = time / duration;
    var percent  = Math.ceil(fraction * 100);

    var progress = document.querySelector('#progressBar');

    progress.style.width = percent + '%';
    document.querySelector('#progressBar').style.width=percent + "%";

}

document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        $('#loader-container').hide();
    }
};

$(document).ready(function () {

    $('#pour_vous_tuile').hover(function () {
        $('#pour_vous_tuile .tuile_home_panel').parent().css( "display", "none" );

    });

    $('#pour_vous_tuile').mouseleave(function () {
        $('#pour_vous_tuile .tuile_home_panel').parent().css( "display", "block" );

    });

    $('.search_bloc input').click(function () {
        $('#pour_vous_tuile .tuile_home_panel').parent().css( "display", "block" );

    });

    $('#start_audio').click(function () {
        var x = document.getElementById("myAudio");
        x.play();
        $('#start_audio').hide();
        $('#stop_audio').show();
    });

    $('#stop_audio').click(function () {
        var x = document.getElementById("myAudio");
        x.pause();
        $('#start_audio').show();
        $('#stop_audio').hide();
    });

    $('.top_link').click(function () {
        $('.page').hide();
        $('#loader-container').show();
        setTimeout(function(){
            $('#loader-container').hide();
            $('#top_playlists').show();
        }, 1000);

    });

    $('#home_link').click(function () {
        $('.page').hide();
        $('#loader-container').show();
        setTimeout(function(){
            $('#loader-container').hide();
            $('#discovery_home').show();
        }, 1000);

    });


    $('.new_link').click(function () {
        $('.page').hide();
        $('#loader-container').show();
        $.ajax({
            url: API_TRACKS_BASE_URL + "?date=desc&limit=12",
            contentType: 'application/json',
            timeout: 10000,
            success: function(tracks){

                $('#discovery_new').children('.news').remove();

                $.each(tracks, function(index, track) {
                    $('#discovery_new').append(trackComponent(track));
                });

                $('#loader-container').hide();
                $('#discovery_new').show();

                reloadListeners();
            }});
    });

    $( "#inscription_user" ).submit(function( event ) {
        var fullname = $('#full_name_reg').val();
        var username = $('#username_reg').val();
        var email = $('#email_reg').val();
        var password = $('#pw_reg').val();
        

        if ( $("#pw_reg").val() === $("#cpw_reg").val()) {
            $("#message_reg").text("Valider ...").show();
            $.ajax({
                type: 'POST',
                url: "http://127.0.0.1:8001/adduser",
                data: {
                    fullname: fullname,
                    username: username,
                    email: email,
                    password: password
                },
                success: function(data, status){
                if (status === 201){
                    alert('sa marche ');
                }}
            }) 
            return false;
            
        }
        else {

            $("#message_reg").text("Les mots de passes ne correspondent pas").show().fadeOut( 1000 );
            return false;
        }
      });

    $('.playlist_track').click(function () {
        $('.page').hide();
        $('#loader-container').show();
       
        var track_id = $(this).attr('data-playlist-id');
        var user_id = $(this).attr('data-user-id');
        $('#playlist_track_body').children().remove();
        $.ajax({
            url : API_PLAYLIST_TRACK_BASE_URL + "/" + track_id,
            contentType: 'application/json',
            timeout: 10000,
            success: function(playlistTracks) {
                
                $.each(playlistTracks['playlistTracks'], function(i, track){

                    $('#playlist_track_body').append(playlistTrackComponent(track));
                })
            }
        })
    });

    $('.artist_link').click(function (e) {

        e.preventDefault();

        $('.page').hide();
        $('#loader-container').show();
        $.ajax({
            url: API_USER_BASE_URL + $(this).attr('data-user-id'),
            contentType: 'application/json',
            timeout: 10000,
            success: function(userInfos){

                $('#artist_dtls h3').html(userInfos['user']['username']);

                $('#page_artist_son').children().remove('.track');
                $('#page_artist_playlist').children().remove('.playlist');

                $.each(userInfos['tracks'], function(index, track) {
                    $('#page_artist_son').append(trackComponent(track, 150));
                });

                $.each(userInfos['playlists'], function(index, pl) {
                    $('#page_artist_playlist').append(playlistComponent(pl));
                });

                $('#loader-container').hide();
                $('#page_artist_son').show();


                reloadListeners();
            }});
    });

    $('#active_live_section').click(function () {
        $('.live_section_screen').hide();
        $('#loader-container-live').show();
        setTimeout(function(){
            $('#loader-container-live').hide();
            $('#live_section_screen_live').show();
        }, 1000);
    });


    $('#active_message_section').click(function () {
        $('.live_section_screen').hide();
        $('#loader-container-live').show();
        setTimeout(function(){
            $('#loader-container-live').hide();
            $('#live_section_screen_message').show();
        }, 1000);
    });

    $('#go_to_inscription_panel').click(function () {
        $('#connexion_popup_panel').hide();
        $('#inscription_popup_panel').show();
    });

    $('#go_to_connexion_panel').click(function () {
        $('#inscription_popup_panel').hide();
        $('#connexion_popup_panel').show();
    });


    $('#connexion_link').hover(function() {
        $(this).css('cursor','pointer');
    });

    $('#go_to_page_actu').click(function () {
        $('.page_dtls-artist').hide();
        $('#loader-container').show();
        setTimeout(function(){
            $('#loader-container').hide();
            $('#page_artist_actu').show();
        }, 1000);
    });

    $('#go_to_page_son').click(function (e) {

        e.preventDefault();

        $('.page_dtls-artist').hide();
        $('#page_artist_son').show();
        // setTimeout(function(){
        //     $('#loader-container').hide();
        //     $('#go_to_page_son').show();
        // }, 1000);
    });

    $('#go_to_page_playlist').click(function (e) {

        e.preventDefault();

        $('.page_dtls-artist').hide();
        $('#page_artist_playlist').show();
        // setTimeout(function(){
        //     $('#loader-container').hide();
        //     $('#page_artist_playlist').show();
        // }, 1000);
    });

    $('.play_this_track').click(function(e) {

        e.preventDefault();

        $('#start_audio').hide();
        $('#stop_audio').show();

        $.ajax({
            url: API_TRACK_BASE_URL + $(this).attr('data-track-id'),
            contentType: 'application/json',
            timeout: 10000,
            success: function(track){

                var trackComments = track['comments'];

                fillCommentSection(track['comments']);

                $('#myAudio').children('source').remove();

                var trackurl = track[0]['track'];
                var tracktype = AUDIO_TYPE_MAPPING[trackurl.split('.').pop().toLowerCase()];

                var source = $('<source/>').attr({
                    src: trackurl,
                    type: tracktype
                });

                $('#player-track-cover').attr('src', track[0]['cover']);

                $('#myAudio').append(source);
                $('#myAudio')[0].load();
                $('#myAudio')[0].play();

                $('#stop_audio').show();
                $('#start_audio').hide();

                incPlayedTime(track[0]['track_id']);

            }});



    });
    $('.set_playlists').click(function () {
        $('.admin_choice').hide();
        $('#admin_set_playlists').show();
    });
    // $('#go_to_page_followers').click(function () {
    //     $('.page_dtls-artist').hide();
    //     $('#loader-container').show();
    //     setTimeout(function(){
    //         $('#loader-container').hide();
    //         $('#page_artist_followers').show();
    //     }, 1000);
    // });
    //
    // $('#go_to_page_following').click(function () {
    //     $('.page_dtls-artist').hide();
    //     $('#loader-container').show();
    //     setTimeout(function(){
    //         $('#loader-container').hide();
    //         $('#page_artist_following').show();
    //     }, 1000);
    // });

    var reloadListeners = function () {

        var current = $('#js_javascript');

        var newScript = $('<script></script>').attr({src: current.attr('src'), id: 'js_javascript'});

        newScript.insertAfter(current);

        current.remove();
    };

    var incPlayedTime = function(track){

        var request = new XMLHttpRequest();
        request.open('PATCH', API_TRACK_PLAYED_BASE_URL + track, false);
        request.setRequestHeader("Content-type","application/json");
        request.send('{"isActive": 1}');
    };

    var fillCommentSection = function(comments) {

        $('#live_section_screen_live').children('.comment').remove();

        $.each(comments, function (i, comment){

            $('#live_section_screen_live').append(commentComponent(comment));
        });
    }
});