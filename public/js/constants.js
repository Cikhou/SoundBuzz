
// API ROUTES //

const API_TRACKS_BASE_URL = 'http://127.0.0.1:8001/tracks';
const API_TRACK_BASE_URL = 'http://127.0.0.1:8001/track/';
const API_TRACK_PLAYED_BASE_URL = 'http://127.0.0.1:8001/addplayedtime/';
const API_USER_BASE_URL = 'http://127.0.0.1:8001/user/';
const API_PLAYLISTS_BASE_URL = 'http://127.0.0.1:8001/playlists';
const API_PLAYLIST_TRACK_BASE_URL = 'http://127.0.0.1:8001/playlisttracks/';
const API_TRACK_VALIDATE_BASE_URL = 'http://127.0.0.1:8001/validatetrack/';
const API_TRACK_REMOVE_BASE_URL = 'http://127.0.0.1:8001/removetrack/';
const API_INVALID_TRACKS_URL = 'http://127.0.0.1:8001/invalidtracks';
const API_VALID_TRACKS_URL = 'http://127.0.0.1:8001/validtracks';


// AUDIO TYPE //

const AUDIO_TYPE_MAPPING = {
    'mp3':'audio/mpeg',
    'mpga':'audio/mpeg',
    'ogg':'audio/ogg',
    'wav':'audio/wav'
};