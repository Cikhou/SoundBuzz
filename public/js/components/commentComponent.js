window.commentComponent = function(comment) {

    var commentDiv = $('<div></div>').addClass('comment');

    var commentDateDiv = $('<div></div>').addClass('comment-date').html(comment['date']);

    var commentOwnerSpan = $('<span></span>').addClass('comment-username').html(comment['username'] + ': ');

    var commentContentSpan = $('<span></span>').addClass('comment-content').html(comment['content']);

    commentDiv.append(commentDateDiv).append(commentOwnerSpan).append(commentContentSpan);

    return commentDiv;

};