var adminTrackComponent = function(track) {

    var trackTr = $('<tr></tr>').attr('id', 'tr-track' + track['track_id']);

    var trackOwnerTd = $('<td></td>').html(track['owner']);

    var trackIdTd = $('<td></td>').html(track['track_id']);

    var trackCreatedAtTd = $('<td></td>').html(track['created_at']);

    var validButton = $('<td></td>').append($('<button></button>')
        .addClass('btn btn-succes')
        .attr({'data-track-id': track['track_id'], type: 'button'})
        .html('Valider')
        .click(function() {
            $.ajax({
                url: API_TRACK_VALIDATE_BASE_URL + $(this).attr('data-track-id'),
                timeout: 10000,
                success: function(track){

                    $('#tr-track' + track['track_id']).remove();

                }});
        })
    );

    var deleteButton = $('<td></td>').append($('<button></button>')
        .addClass('btn btn-succes')
        .attr({'data-track-id': track['track_id'], type: 'button'})
        .html('Supprimer')
        .click(function() {
            $('#tr-track' + track['track_id']).remove();
        })
    );

    trackTr.append(trackIdTd).append(trackOwnerTd).append(trackCreatedAtTd).append(validButton).append(deleteButton);

    return trackTr;
};