window.playlistComponent = function(playlist) {

    var playlistDiv = $('<div></div>').attr({class: 'playlist'});

    var playlistTitle = $('<a></a>')
        .attr({'data-playlist-id': playlist['playlist_id']})
        .addClass('playlist-title')
        .html(playlist['title'] + ' : ');

    var playlistDesc = $('<span></span>')
        .addClass('playlist-desc')
        .html(playlist['description']);

    playlistDiv.append(playlistTitle).append(playlistDesc);

    return playlistDiv;
};