window.trackComponent = function(track, coverSize) {

    var coverSize = coverSize || 300;
    var trackDiv = $('<div></div>').addClass('news track col-3');

    var cover = $('<a></a>').addClass('play_this_track').attr('data-track-id', track['track_id']);
    var coverImg = $('<img/>').attr({
          'src': track['cover'],
          'width': coverSize,
          'height': coverSize
      });

    cover.append(coverImg);

    var trackInfos = $('<div></div>');

    var trackGenre = $('<span></span>').addClass('trackgenre').html(track['genre'] + ': ');

    var trackName = $('<span></span>').addClass('trackname').html(track['title']);

    trackInfos.append(trackGenre).append(trackName);

    var artistName = $('<p></p>').addClass('tracklistname').append(
      $('<a></a>').addClass('artist_link').attr({
          'data-user-id': track['owner_id'],
          'href' : void(0)
      }).html(track["owner"]).click(function () {
          $('.page').hide();
          $('#loader-container').show();
          setTimeout(function(){
              $('#loader-container').hide();
              $('#artist').show();
          }, 1000);
      })
    );

    trackDiv.append(cover).append(trackInfos).append(artistName);

    return trackDiv;

};