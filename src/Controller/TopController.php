<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TopController extends Controller
{
    public function view()
    {
      return $this->render('top.html.twig',array('User'=>"Cikhou"));
     }
}
