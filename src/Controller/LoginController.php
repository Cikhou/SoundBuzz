<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Unirest\Request\Body as URequestBody;
use Unirest\Request as URequest;

class LoginController extends Controller
{

    public function loginAction(Request $req)
    {
        // just setup a fresh $task object (remove the dummy data)
        $defaultArray = array();

        $form = $this->createFormBuilder($defaultArray)
            ->add('username', TextType::class)
            ->add('password', PasswordType::class)
            ->add('Connection', SubmitType::class, array('label' => 'Connection'))
            ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $headers = array('Accept' => 'application/json');

            $body = URequestBody::form($form->getData());


            $response = URequest::post('http://localhost:8001/login', $headers, $body);

            if ($response->code === 200) {

                $this->get('session')->set('user', json_decode($response->raw_body,true));

                return $this->redirectToRoute('app_homepage');
            }

            switch ($response->code) {

                case 404:
                    $form->get('username')->addError(new FormError('identifiant inconnu'));
                    break;

                case 403:
                    $form->get('password')->addError(new FormError('mot de passe incorrect'));
                    break;

                default:

                    return new Response('<h1>Erreur Inconnue....</h1>');
            }

            return $this->render('login/login_form.html.twig', array(
                'form' => $form->createView(),
            ));
        }

        return $this->render('login/login_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function logoutAction() {

        $this->get('session')->remove('user');

        return $this->redirectToRoute('app_homepage');
    }
}
