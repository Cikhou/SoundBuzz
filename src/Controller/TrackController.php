<?php

namespace App\Controller;


use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Unirest\Request as URequest;
use Unirest\Request\Body as URequestBody;

class TrackController extends Controller
{

    public function addAction(Request $req)
    {

        if($this->get('session')->get('user') === null) {

           return $this->redirectToRoute('app_login');
        }

        // just setup a fresh $task object (remove the dummy data)
        $defaultArray = array();

        $form = $this->createFormBuilder($defaultArray)
            ->add('title', TextType::class, array(
                'label' => 'Titre',
                'required'  => true,
            ))
            ->add('genre', ChoiceType::class, array(
                'choices'  => array(
                    'Musique Classique' => "Musique Classique",
                    'Electro' => 'Electro',
                    'Jazz' => 'Jazz',
                    'R&B' => 'R&B'
                ),
                'placeholder' => "Style musical",
                'required' => true
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Description',
                'required'  => true,
            ))
            ->add('picture', FileType::class, array(
                'label' => 'Couverture',
                'required'  => true,
            ))
            ->add('track', FileType::class, array(
                'label' => 'Fichier audio',
                'required'  => true,
            ))
            ->add('downloadable', CheckboxType::class, array(
                'data' => false
            ))
            ->add('explicit', CheckboxType::class, array(
                'data' => false
            ))
            ->add('user_id', HiddenType::class, array(
                'data' => $this->get('session')->get('user')['user_id']))
            ->add('Envoyer ma musique', SubmitType::class, array('label' => 'Envoyer ma musique'))
            ->getForm();


        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $_data = $form->getData();

            var_dump($_data);

            $picture = $_data['picture'];
            $pictureName = md5(uniqid()).'.'.$picture->guessExtension();
            $picturePath = $this->getParameter('public_img_path') . $pictureName;

            $track = $_data['track'];
            $trackName = md5(uniqid()).'.'. $track->guessExtension();
            $trackPath = $this->getParameter('public_track_path') . $trackName;


            // moves the file to the directory where brochures are stored
            $picture->move($this->getParameter('img_directory'), $pictureName);

            $track->move(
                $this->getParameter('track_directory'),
                $trackName
            );

            $_data['track'] = $trackPath;
            $_data['picture'] = $picturePath;

            $headers = array('Accept' => 'application/json');

            $body = URequestBody::form($_data);


            $response = URequest::post('http://localhost:8001/addtrack', $headers, $body);

            var_dump($response->code);

            if ($response->code === 201) {

                return $this->redirectToRoute('app_homepage');
            }


        }

        return $this->render('track/new_track_form.html.twig', array(
            'form' => $form->createView(),
        ));

    }
}
