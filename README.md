Soundbuzz

symfony 4 soundbuzz Client side 
Author : Ezechiel Kounougnan - IPSI ASI D12

Prerequisites:

PHP ^7 needed for symfony 4 

Composer 

- composer install


Installing

clone the repository via http

https://gitlab.com/Cikhou/SoundBuzz.git

Clone via ssh

git@gitlab.com:Cikhou/SoundBuzz.git

install composer if needed 
"php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');""

